import React from "react";
import { links } from "./root.helper.js";
import { Link } from "@reach/router";
import Button from "@material-ui/core/Button";
import Appbar from "./component/Appbar";
import Sidenav from "./component/Sidenav";
export default function Root(props) {
  return (
    <div className="h-16 flex items-center justify-between px-6 bg-primary text-white">
      <Sidenav />
      {/* <div className="flex items-center justify-between">
        {links.map((link) => {
          return (
            <Link key={link.href} className="p-6" to={link.href}>
              {link.name}
            </Link>
          );
        })}
      </div> */}
    </div>
  );
}
