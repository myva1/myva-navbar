export const links = [
  {
    name: "Home",
    href: "/home",
  },
  {
    name: "Task Manager",
    href: "/taskManager",
  },
];
